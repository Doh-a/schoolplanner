# SchoolPlanner

Un petit outil pour afficher sur un site web les devoirs que les enfants doivent faire, et pour n'afficher qu'une version réduite de ces devoirs aux enfants pour quand ils le recopient dans leurs journaux de classe.